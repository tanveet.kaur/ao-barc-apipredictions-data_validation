import csv
import pymysql
import MySQLdb
import configparser
from openpyxl import load_workbook
import xlsxwriter
import sys
import pandas as pd
import os

import sys
import time
import calendar
import shutil
# import uuid
from datetime import timedelta
from datetime import datetime
from sqlalchemy import create_engine
# import email_script_for_barc.send_mail as mail
def tt(a):
    b = str(a).split(':')
    # print (b)
    j=3600
    k=0
    for i in range(len(b)):
        k=k+float(b[i])*j
        j=j/60
    return k



    #   return int(b[0]) * 3600 + int(b[1]) * 60 + float(b[2])

def get_pred_db_connection(stage):
    '''this nethod will return database connection if connection not exist it will return new connection 
    else return exist connection '''
    #read database credentials for environment variable; 
    config = configparser.ConfigParser()
    config.read('/var/.ao/barcdb-parameters.ini')

    connection = MySQLdb.connect(unix_socket='/cloudsql/'+config.get(stage,'INSTANCE_NAME'),  # your host
                user=config.get(stage,'DB_USERNAME'),  # username
                passwd=config.get(stage,'DB_PASSWORD'),  # password
                db=config.get(stage,'DB_NAME'))
    print ("new created con object ")    
    return connection
def get_ui_db_connection():
    config1 = configparser.RawConfigParser()
    config1.read('/var/.ao/uidb-parameters.ini')
    STAG2 = config1.get('DEFINITION', 'SECTION').upper()
    INSTANCE_NAME = config1.get(STAG2, 'INSTANCE_NAME')  # your host
    USER = config1.get(STAG2, 'DB_USERNAME')  # username
    PASSWD = config1.get(STAG2, 'DB_PASSWORD')  # password
    DB_NAME = config1.get(STAG2, 'DB_NAME') # database name
    
    ENGINE = create_engine(
            "mysql+pymysql://{0}:{1}@/{2}?unix_socket=/cloudsql/{3}".format(USER, PASSWD, DB_NAME,INSTANCE_NAME))
    return ENGINE
def get_ui_backend_connection(stage):
    '''this nethod will return database connection if connection not exist it will return new connection 
    else return exist connection '''
    #read database credentials for environment variable; 
    config = configparser.ConfigParser()
    config.read('/var/.ao/barcdb-parameters.ini')
    connection = None
    DEFINATION_SECTION_NAME="UI"
    host = config.get(DEFINATION_SECTION_NAME, 'DB_HOST')
    username =  config.get(DEFINATION_SECTION_NAME, 'DB_USERNAME')
    password = config.get(DEFINATION_SECTION_NAME, 'DB_PASSWORD')
    name = config.get(DEFINATION_SECTION_NAME, 'DB_NAME')
    connection = pymysql.connect(host,username,password,name)
    print ("new created con object ")          
    return connection
    
def get_data_from_db(sql_query,connection):
    #read database credentials for environment variable;
    df = pd.read_sql(sql_query, connection)
    return df
def get_data_barc(stage,d):
    con = get_pred_db_connection(stage)
    sql_query='select request_id, api_requested, status,output_url,video_duration, count(*) as count from request_tracking where SUBSTRING(request_id,1,8)="{}" group by request_id, api_requested, status,output_url,video_duration;'.format(d)
    df = get_data_from_db(sql_query,con)  
    return df
def get_data_archive_proc_retry(stage,d):
    con = get_pred_db_connection(stage)
    sql_query='select request_id,process_id from archive_process_retry where request_id in {}'.format(d)
    df = get_data_from_db(sql_query,con)  
    return df
def get_data_archive_proc_limit(stage,d):
    con = get_pred_db_connection(stage)
    sql_query='select process_id,count from process_limit where process_id in {}'.format(d)
    df = get_data_from_db(sql_query,con)  
    return df
def get_channel_connection(stage,d):
    con = get_ui_backend_connection(stage)
    sql_query='select channel_name,channel_value from channel where channel_value in {}'.format(d)
    df = get_data_from_db(sql_query,con)  
    return df
def barc_week(date_passed):
    '''
    checking the current date and making changes in whe week start and end time to match with barc week
    '''

    current_date = datetime.strptime(date_passed, '%Y-%m-%d') + timedelta(days=-7)
    print ("----")
    print (current_date)

    # current_date= datetime.strptime(current_date, '%Y-%m-%d')
    print (current_date)
    week_day = calendar.day_name[current_date.weekday()]
    print (week_day)
    if week_day == 'Monday':
        week_start = current_date + timedelta(days=-2)
        week_end = current_date + timedelta(days=4)
     
    elif week_day == 'Tuesday':
        week_start = current_date + timedelta(days=-3)
        week_end = current_date + timedelta(days=3)
  
    elif week_day == 'Wednesday':
        week_start = current_date + timedelta(days=-4)
        week_end = current_date + timedelta(days=2)
     
    elif week_day == 'Thursday':
        week_start = current_date + timedelta(days=-5)
        week_end = current_date + timedelta(days=1)
    
    elif week_day == 'Friday':
        week_start = current_date + timedelta(days=-6)
        week_end = current_date + timedelta(days=0)
   
    elif week_day == 'Saturday':
        week_start = current_date + timedelta(days=0)
        week_end = current_date + timedelta(days=6)
    
    else:
        week_start = current_date + timedelta(days=1)
        week_end = current_date + timedelta(days=5)
        
    l=[]
    for i in range(0,7):
        k=str(datetime.strftime(week_start,'%Y-%m-%d'))
        k=k.replace("-","")
        l.append(k)
        week_start=week_start+timedelta(days=1)
    print ("l",l)

    
    current_date = str(datetime.strftime(current_date,'%Y-%m-%d'))
    week_start = str(datetime.strftime(week_start,'%Y-%m-%d'))
    week_end = str(datetime.strftime(week_end,'%Y-%m-%d'))


    return l
def checks(a):
    if a!=None:
        return 1
    else:
        return 0
def return_api_requested(a):
    return str(str(a).split("_")[-1])
def checks_status(a):
    if a=="Done":
        return 1
    else:
        return 0
def return_str(a):
    return a[0:len(a)-2]
def return_date(a):
    return a[0:8]
def return_channel(a):
    return a[9:10]
def return_slot_start(a):
    return a[11:32]
def return_datetime(a):
    k=datetime.strptime(str(a), "%Y%m%d")
    return k
def return_retries(a):
    if a=="NA":
        k=""
    else:
        k=int(a)+1
    return k
def return_int(x):
    print (x)
    if str(x)=="NA":
        return None
    else:
        return int(x)
def main():
    stage = sys.argv[1]
    #check if the date is passed manually
    #if not passed, take the current date
    if len(sys.argv) > 2:
        date_passed = sys.argv[2]
    else:
        date_passed = str(datetime.today())[0:10]
        

    #find the barc week of the date
    l=barc_week(date_passed)
    df=pd.DataFrame(columns=['request_id', 'api_requested', 'status','output_url','video_duration', 'count'])
    
    #fetch data date wise from request tracking table
    
    for i in l:

        df_temp=get_data_barc(stage,i)
        df=df.append(df_temp)
    
    df=df.reset_index()
    
    #proceed only if data is present in the database
    if len(df)>0:
        #create a flag for output url
        df['F_Output']=df['output_url'].apply(lambda x: checks(x))
        #create a flag for status
        df['F_Done']=df['status'].apply(lambda x: checks_status(x))
        df['successful_api']=df['F_Output']*df['F_Done']
        unique_req_ids=tuple(df['request_id'].unique())
        df_process_retry=get_data_archive_proc_retry(stage,unique_req_ids)
        # df_process_retry.to_csv("/home/user/BARC_DATA/df_process_retry_Prediction_check_v1_2612.csv")
        
        df_process_retry['api_requested']=df_process_retry['process_id'].apply(lambda x: return_api_requested(x))
        df_process_retry=df_process_retry.drop_duplicates()
        unique_process_ids=tuple(df_process_retry['process_id'].unique())
        df_process_limit=get_data_archive_proc_limit(stage,unique_process_ids)
        df_process_merged=pd.merge(df_process_retry,df_process_limit, on='process_id',how='left')
        df_process_merged['count']=df_process_merged['count'].fillna("0")
        df_process_merged['count']=df_process_merged['count'].astype(int)
        df_process_merged=df_process_merged.rename(columns={'count':'retries'})
        
        print (df_process_merged.columns.values.tolist())
        grouped=df_process_merged.groupby(['request_id','api_requested'])['retries'].sum()
        df_process_merged= pd.DataFrame(grouped)
        df_process_merged=df_process_merged.reset_index()
        # df_process_merged.to_csv("/home/user/BARC_DATA/df_process_mergedPrediction_check_v1_2612.csv")
        df_process_merged['api_requested']=df_process_merged['api_requested'].str.upper()

        df_proc_retry=pd.merge(df,df_process_merged,on=['request_id','api_requested'],how='left')
        
        df_proc_retry["date"]=df_proc_retry['request_id'].apply(lambda x: return_date(x))
        df_proc_retry["channel"]=df_proc_retry['request_id'].apply(lambda x: return_channel(x))
        df_proc_retry["slot_time"]=df_proc_retry['request_id'].apply(lambda x: return_slot_start(x))
        # df_proc_retry["slot_end"]=df_proc_retry['request_id'].apply(lambda x: return_slot_end(x))
        #1 if video is not success, 0 if video is success at api/collation level
        df_proc_retry["v2c_collation_success"]=1
        df_proc_retry['retries']=df_proc_retry['retries'].fillna("NA")
        for index,row in df_proc_retry.iterrows():
            if row['successful_api']==0:
                if row['retries']=='NA':
                    print ("------")
                    df_proc_retry.loc[index,'v2c_collation_success']=0           
        df_proc_retry['final_processing_result']=df_proc_retry["v2c_collation_success"]*df['successful_api']
        unique_channel_id=tuple(df_proc_retry['channel'].unique())
        df_channel=get_channel_connection(stage,unique_channel_id)
        print ("-----")
        print (df_channel.columns.values.tolist())
        print ("------")
        df_channel=df_channel.rename(columns={'channel_value':'channel'})
        df_channel=df_channel.astype(str)
        df_proc_retry=pd.merge(df_proc_retry,df_channel, on='channel', how='left' )
        df_proc_retry['date']=df_proc_retry['date'].apply(lambda x: return_datetime(x))
        df_proc_retry['No of processed times']=df_proc_retry['retries'].apply(lambda x: return_retries(x))
        df_proc_retry['video_duration']=df_proc_retry['video_duration'].astype(str)
        df_proc_retry['video_duration']=df_proc_retry['video_duration'].apply(lambda x: tt(x))
        df_proc_retry['final_processing_result_temp']=(df_proc_retry['final_processing_result']-1)*(-1)
        df_proc_retry_temp=df_proc_retry.groupby(['request_id'])['final_processing_result_temp'].max()
        df_proc_retry_temp= pd.DataFrame(df_proc_retry_temp)
        print (df_proc_retry.columns.values.tolist())
        print ("------")
        df_proc_retry=pd.merge(df_proc_retry,df_proc_retry_temp, on='request_id', how='left')
        df_proc_retry=df_proc_retry.drop('final_processing_result_temp_x',axis=1)
        df_proc_retry['final_processing_result_temp_y']=(df_proc_retry['final_processing_result_temp_y']-1)*(-1)
        df_proc_retry=df_proc_retry.rename(columns={'final_processing_result_temp_y':'video_level_proc_result'})
        df_proc_retry.to_csv("/home/user/BARC_DATA/df_proc_retryPrediction_check_v1_2612.csv") 
        df_final= df_proc_retry.filter(['request_id', 'api_requested','successful_api', 'channel_name','date','slot_time','video_duration', 'v2c_collation_success', 'final_processing_result', 'No of processed times','video_level_proc_result'], axis=1)    

        df_final['No of processed times']=df_final['No of processed times'].astype(str)
        df_final['No of processed times'] = pd.to_numeric(df_final['No of processed times'], errors='coerce')
        df_final=df_final.rename(columns={'No of processed times':'No_of_processed_times'})
        df_final.to_csv("/home/user/BARC_DATA/df_finalPrediction_check_v1_2612.csv") 
        engine = get_ui_db_connection()
        with engine.connect() as conn, conn.begin():
            df_final.to_sql('api_predictions', conn, if_exists='append', index=False)
    else:
        print ("No data for this week")
main()