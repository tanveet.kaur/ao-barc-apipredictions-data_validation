 AO-Project-Metadata-Accuracy-QC-Script
==========

Prerequisites
------
* python==**3.5** 
* pip==**9.0.1**
* pandas==**0.20.3**
* pygsheets==**2.0.0**
* PyMySQL==**0.9.2**


<br><br/>

## Installation Instructions
```
pip install -r requirement.txt
apt-get update
```

## To Run this script,
*Note:*
    1. Conguire the constants in constant.py according to the current specification(check the constant.py file for further understanding).
    2. Make sure python3 is preinstalled.

Follow the following steps to run the script-

```sh
pip install requirements.txt
```
## For running the script, follow these steps:
```
sudo python3 "-----filename-----" "BARC" date
```
If date is specified,code will take the predictions for the corresponding previous barc week, but if no date is specified, it takes the current date.


<br><br/>
